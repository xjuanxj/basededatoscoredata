//
//  UserTableViewCell.swift
//  BDconCoreData
//
//  Created by Juan Sebastian Benavides Cardenas on 2/2/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phonelbl: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(_ persona:Person)
    {
        nameLbl.text = persona.name ?? "ND"
        addressLbl.text = persona.address ?? "ND"
        phonelbl.text = persona.phone ?? "ND"
    }

}
