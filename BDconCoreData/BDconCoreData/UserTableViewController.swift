//
//  UserTableViewController.swift
//  BDconCoreData
//
//  Created by Juan Sebastian Benavides Cardenas on 2/2/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import CoreData
class UserTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{

    
    @IBOutlet weak var UserTable: UITableView!
    

    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var personaArray:[Person] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func numberOfSections(in tableView: UITableView) -> Int { // metodo para hacer la tabla
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return personaArray.count //igual al lenght de java en arrays
        default:
            return 5
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell
    {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserTableViewCell
            cell.fillData(personaArray[indexPath.row])
            //cell.textLabel?.text = pokemonArray[indexPath.row].pkName //como
            return cell
            
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        flechAll()
        UserTable.reloadData()
    }
    func flechAll()
    {
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        do {
            
            let results = try managedObjectContext.fetch(request)
            personaArray = results
            for p in results
            {
                let person = p
                //print ("Nombre: \(person.name!)", terminator: " ")
                //print ("Dirección: \(person.address!)", terminator: " ")
                //print ("Telefono: \(person.phone!)", terminator: " ")
                
                
                print("")
            }
            
        } catch  {
            print("Error al retornar")
        }
    }

    
    

}
