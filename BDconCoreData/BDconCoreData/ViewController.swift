//
//  ViewController.swift
//  BDconCoreData
//
//  Created by Juan Sebastian Benavides Cardenas on 26/1/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    var persona:Person?
    //uno solo y siempre se crea en el app delefate
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



    @IBAction func saveButtonPressed(_ sender: Any) {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: managedObjectContext)
        let person  = Person(entity: entityDescription!,insertInto: managedObjectContext)
        person.name = nameTextField.text!
        person.address = addressTextField.text!
        person.phone = phoneTextField.text!
        do
        {
           try managedObjectContext.save()
            clearFields()
        }
        catch
        {
            print("No se insertar los datos en coreData")
        }
        

        
        
        
    }
    func clearFields()
    {
        nameTextField.text! = ""
        addressTextField.text! = ""
        phoneTextField.text! = ""
    }
    @IBAction func findButtonPressed(_ sender: Any) {
        
       if nameTextField.text! == ""
       {
            flechAll()
            performSegue(withIdentifier: "findAllSegue", sender: self)
        return
       }
        //select para este framework
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        let pred = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = pred
        do {
            let results = try managedObjectContext.fetch(request)
            if results.count > 0
            {
                let person = results[0] 
                print ("Nombre: \(person.name!)", terminator: " ")
                print ("Dirección: \(person.address!)", terminator: " ")
                print ("Telefono: \(person.phone!)", terminator: " ")
                
                //addressTextField.text = "\(person.address!)"
                //phoneTextField.text = "\(person.phone!)"
                persona = person
                performSegue(withIdentifier: "findOneSegue", sender: self)
                
                print("")
            }
        } catch  let error{
            print(error)
        }
        
    }
    func flechAll()
    {
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        do {
            
            let results = try managedObjectContext.fetch(request)
            for p in results
            {
                let person = p
                //print ("Nombre: \(person.name!)", terminator: " ")
                //print ("Dirección: \(person.address!)", terminator: " ")
                //print ("Telefono: \(person.phone!)", terminator: " ")
                persona = person
                
                print("")
            }
            
        } catch  {
            print("Error al retornar")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "findOneSegue")
        {
            let destination = segue.destination as! FindOneViewController
            destination.persona = persona
        }
    }
}

