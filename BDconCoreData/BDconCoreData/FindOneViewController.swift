//
//  FindOneViewController.swift
//  BDconCoreData
//
//  Created by Juan Sebastian Benavides Cardenas on 2/2/18.
//  Copyright © 2018 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import CoreData
class FindOneViewController: UIViewController {

    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var phone: UILabel!
    private let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var persona:Person?
    override func viewDidLoad() {
        super.viewDidLoad()
        title = persona?.name!
        phone.text = persona?.phone ?? "ND"
        addressLbl.text = persona?.address ?? "ND"
        
    }
    
    
    
    @IBAction func Borrar(_ sender: Any)
    {
        managedObjectContext.delete(persona!)
        do {
            try managedObjectContext.save()
            //regresar en el view Controller
            navigationController?.popViewController(animated: true)
        } catch  {
            print()
        }
        
    }
    
}
